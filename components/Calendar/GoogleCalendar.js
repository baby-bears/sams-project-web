import React, { useEffect, useState } from "react";
import Calendar from "@ericz1803/react-google-calendar";
import { css } from "@emotion/react";

const API_KEY = "AIzaSyCe5J4iPxuoXdTqCPNyI6VzD1aQfKlbM-E";
const calendars = [
  {
    calendarId: "sams.kmitl@gmail.com",
    //color: "#ffffff", //optional, specify color of calendar 2 events
  },
];

let styles = {
  // //you can use object styles (no import required)
  // calendar: {
  //   borderWidth: "3px", //make outer edge of calendar thicker
  // },

  event: css`
    color: black;
    // border: 1px solid black;
    // margin-top: 5px
  `,

  eventText: css`
    color: black;
  `,

  multiEvent: css`
    color: black;
    //border: 1px solid black;
    // margin-top: 5px
  `,

  eventCircle: css`
    // color: black;
    // margin-bottom: 5px
  `,

  //you can also use emotion's string styles
  // today: css`
  //  /* highlight today by making the text red and giving it a red border */
  //   color: red;
  //   border: 1px solid red;
  // `
};

const GoogleCalendar = () => {
  return (
    <div className="Main-Toggle-Div">
      <Calendar apiKey={API_KEY} calendars={calendars} styles={styles} />
    </div>
  );
};

export default GoogleCalendar;
