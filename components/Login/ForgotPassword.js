import React, { useEffect, useState, useContext } from "react";
import { useMutation } from "@apollo/react-hooks";
import { onError } from "@apollo/client/link/error";
import gql from "graphql-tag";
import Cookies from "js-cookie";
import Router from "next/router";
import { AuthContext } from "../../appState/AuthProvider";


const FORGOTPASS = gql`
  mutation ForgotPass($email: String!) {
    forgetPassword(input: { email: $email}) {
      success
    }
  }
`;

const forgotPassword = () => {
  const [userInfo, setUserInfo] = useState({
    email: ""
  });

  const { setAuthUser } = useContext(AuthContext);

  const [forgotPassword, { loading, error }] = useMutation(FORGOTPASS, {
    variables: { ...userInfo },
    //เมื่อสำเร็จแล้วจะส่ง data เอามาใช้ได้
    onCompleted: (data) => {
      if (data) {
        setUserInfo({
          email: ""
        });
        Router.push("/newPassword");
      }
    },
  });

  const handleChange = (e) => {
    ////console.log(e.target.value);
    setUserInfo({
      ...userInfo, //copy ค่าจากที่เราพิมพ์เข้าไป
      [e.target.name]: e.target.value,
    });
  };
  const [showError, setErrorShow] = useState("");
  //   //console.log(userInfo);
  //   const [showError, setErrorShow] = useState("");
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      await forgotPassword();
    } catch (error) {
      //console.log(error.message);
      setErrorShow(error.message.split("Error: ").pop());
    }
  };

  return (
    <div className="login_user_card">
      <div className="login_form_container">
        <div className="login_header">
          <h3>เปลี่ยนรหัสผ่าน</h3>
        </div>
        <div>
          <form>
            <div className="login-text">
              <label className="login-Text-newPass">กรุณากรอกอีเมลของคุณเพื่อทำการเปลี่ยนรหัสผ่าน</label>
            </div>
            <div className="login_input mb-3">
              <label htmlFor="username">อีเมล</label>
              <input
                type="email"
                name="email"
                className="login_input-user-pass"
                placeholder="อีเมล"
                value={userInfo.email}
                onChange={handleChange}
                required
              />
              <p className="login-Error-Msg">{showError}</p>
            </div>
            <div className="login_form-group">
              <div className="mt-3">
                <button type="submit" name="button" className="login_btn" onClick={handleSubmit}>
                  ส่ง
                </button>
              </div>

              {/* <div className="d-flex justify-content-center login_links">
              <a href="#">ลืมรหัสผ่าน?</a>
            </div> */}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default forgotPassword;
