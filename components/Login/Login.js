import React, { useEffect, useState, useContext } from "react";
import { useMutation } from "@apollo/react-hooks";
import { onError } from "@apollo/client/link/error";
import gql from "graphql-tag";
import Cookies from "js-cookie";
import Router from "next/router";
import { AuthContext } from "../../appState/AuthProvider";
import { Spinner, Modal } from 'react-bootstrap';

const LOG_IN = gql`
  mutation Login($email: String!, $password: String!) {
    login(input: { email: $email, password: $password }) {
      accessToken
    }
  }
`;

const login = () => {
  const [userInfo, setUserInfo] = useState({
    email: "",
    password: "",
  });


  const [showLoading, setLoadingShow] = useState(false);
  const handleLoadingClose = () => setLoadingShow(false);
  const handleLoadingShow = () => setLoadingShow(true);

  const { setAuthUser } = useContext(AuthContext);

  const [loading, setloading] = useState(false);

  const [login] = useMutation(LOG_IN, {
    variables: { ...userInfo },
    //เมื่อสำเร็จแล้วจะส่ง data เอามาใช้ได้
    onCompleted: (data) => {
      if (data) {
        setAuthUser(data.login.accessToken);
        Cookies.set("jwt", data.login.accessToken); // เอา cookies ไปใส่ใน headers apolloclients
        //console.log(data);
        setUserInfo({
          email: "",
          password: "",
        });
        Router.push("/");
        handleLoadingClose();
        window.location.reload();
      }
    },
  });
  const handleChange = (e) => {
    ////console.log(e.target.value);
    setUserInfo({
      ...userInfo, //copy ค่าจากที่เราพิมพ์เข้าไป
      [e.target.name]: e.target.value,
    });
  };

  //console.log(userInfo);
  const [showError, setErrorShow] = useState("");
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      handleLoadingShow()
      await login();
    } catch (error) {
      handleLoadingClose()
      //console.log(error.message)
      setErrorShow("**อีเมลหรือรหัสผ่านของคุณไม่ถูกต้อง")
    }
  };

  return (
    <div className="login_user_card">
      <div className="login_form_container">
        <div className="login_header">
          <h3>เข้าสู่ระบบ</h3>
        </div>
        <div>
          <form onSubmit={handleSubmit}>
            <div className="login_input mb-3">
              <label htmlFor="username">อีเมล</label>
              <input
                type="email"
                name="email"
                className="login_input-user-pass"
                placeholder="อีเมล"
                value={userInfo.email}
                onChange={handleChange}
                required
              />
            </div>
            <div className="login_input mb-3">
              <label htmlFor="password">รหัสผ่าน</label>
              <input
                type="password"
                name="password"
                className="login_input-user-pass"
                placeholder="รหัสผ่าน"
                value={userInfo.password}
                onChange={handleChange}
                required
              />
              <p className="login-Error-Msg">{showError}</p>
              {/* {showError && <p className="login-Error-Msg">**อีเมลหรือรหัสผ่านของคุณไม่ถูกต้อง</p>} */}
            </div>
            <div className="login_form-group">
              <div className="mt-3">
                <button type="submit" name="button" className="login_btn">
                  เข้าสู่ระบบ
                </button>
              </div>

              <div className="login_links">
                คุณยังไม่มีบัญชี ?{" "}
                <a href="#register" className="login_register" onClick={() => Router.push("/register")}>
                  สมัครบัญชี
                </a><br></br>
                <a className="login_register" onClick={() => Router.push("/forgotPassword")}>ลืมรหัสผ่าน?</a>
              </div>

              {/* <div className="d-flex justify-content-center login_links">
              <a href="#">ลืมรหัสผ่าน?</a>
            </div> */}
            </div>
          </form>
        </div>
      </div>
      <div>
        <Modal
          //   show={showLoading}
          //   onHide={handleLoadingClose}
          // >
          //   <div className="alert alert-success alert-dismissible fade show" role="alert">
          //     <strong>Loading...</strong> <br></br>
          //     {/* <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={handleSuccessClose}>
          //                 <span aria-hidden="true">&times;</span>
          //             </button> */}
          //   </div>
          show={showLoading}
          onHide={handleLoadingClose}
          size="sm"
          aria-labelledby="contained-modal-title-vcenter"
          className="modal-size"
          centered
        >
          <Modal.Header>
            <Modal.Title className="d-flex justify-content-center modal-text-loading" id="contained-modal-title-vcenter">
              กำลังทำการเข้าสู่ระบบ
        </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* <h4>Centered Modal</h4> */}
            <p>
              <Spinner className="d-flex justify-content-center modal-text-loading" animation="border" variant="dark" />
            </p>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
};

export default login;
