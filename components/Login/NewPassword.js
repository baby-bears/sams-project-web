import React, { useEffect, useState, useContext } from "react";
import { useMutation } from "@apollo/react-hooks";
import { onError } from "@apollo/client/link/error";
import gql from "graphql-tag";
import Cookies from "js-cookie";
import Router from "next/router";
import { AuthContext } from "../../appState/AuthProvider";

const CHANGEPASS = gql`
  mutation ChangePass($code: String!, $newPassword: String!) {
    changePassword(input: { code: $code, newPassword: $newPassword}) {
      studentId
    }
  }
`;


const newPassword = () => {
  const [userInfo, setUserInfo] = useState({
    code: "",
    newPassword: "",
  });

  const { setAuthUser } = useContext(AuthContext);

  const [changePassword, { loading, error }] = useMutation(CHANGEPASS, {
    variables: { ...userInfo },
    //เมื่อสำเร็จแล้วจะส่ง data เอามาใช้ได้
    onCompleted: (data) => {
      if (data) {
        setUserInfo({
          code: "",
          newPassword: "",
        });
        Router.push("/login");
      }
    },
  });

    const handleChange = (e) => {
      ////console.log(e.target.value);
      setUserInfo({
        ...userInfo, //copy ค่าจากที่เราพิมพ์เข้าไป
        [e.target.name]: e.target.value,
      });
    };

  //   //console.log(userInfo);
  const [showError, setErrorShow] = useState("");
    const handleSubmit = async (e) => {
      try {
        e.preventDefault();
        await changePassword();
      } catch (error) {
        //console.log(error.message);
        setErrorShow(error.message.split("error: ").pop());
      }
    };

  return (
    <div className="login_user_card">
      <div className="login_form_container">
        <div className="login_header">
          <h3>เปลี่ยนรหัสผ่าน</h3>
        </div>
        <div>
          <form>
            <div className="login_input mb-3">
              <label htmlFor="username">CODE</label>
              <input
                type="text"
                name="code"
                className="login_input-user-pass"
                placeholder="CODE 6 หลัก"
                value={userInfo.code}
                onChange={handleChange}
                required
              />
            </div>
            <div className="login_input mb-3">
              <label htmlFor="password">รหัสผ่าน</label>
              <input
                type="password"
                name="newPassword"
                className="login_input-user-pass"
                placeholder="รหัสผ่าน"
                value={userInfo.newPassword}
                onChange={handleChange}
                required
              />
              <p className="login-Error-Msg">{showError}</p>
              {/* {showError && <p className="login-Error-Msg">**อีเมลหรือรหัสผ่านของคุณไม่ถูกต้อง</p>} */}
            </div>
            <div className="login_form-group">
              <div className="mt-3">
                <button type="submit" name="button" className="login_btn" onClick={handleSubmit}>
                  ยืนยัน
                </button>
              </div>

              {/* <div className="d-flex justify-content-center login_links">
              <a href="#">ลืมรหัสผ่าน?</a>
            </div> */}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default newPassword;
