import React, { useEffect, useState, useContext } from "react";
import { Slide } from "react-slideshow-image";
import { Fade } from "react-slideshow-image";
import Slideimg1 from "../../Image/slideimg5.jpg";
import Slideimg2 from "../../Image/slideimg2.jpg";
import Slideimg3 from "../../Image/slideimg6.jpg";
import Cal from "../../Image/date.png";
import Calendar from "../Calendar/GoogleCalendar";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import { Button, Modal } from "react-bootstrap";

const MainPageSlidebar = () => {
  const [showModalCalendar, setShowModalCalendar] = useState(false);
  const handleCloseModalCalendar = () => setShowModalCalendar(false);
  const handleShowModalCalendar = () => setShowModalCalendar(true);

  return (
    <>
      <div className="Main-Page-Slidebar">
        <Carousel showArrows={true}>
          <div className="Main-Page-Slidebar-Img">
            <img src={Slideimg1} />
          </div>
          <div className="Main-Page-Slidebar-Img">
            <img src={Slideimg2} />
          </div>
          <div className="Main-Page-Slidebar-Img">
            <img src={Slideimg3} />
          </div>
        </Carousel>
      </div>
      <div className="Main-Page-Calendar">
        <div>
          <button
            className="Main-Page-Calendar-Btn"
            onClick={handleShowModalCalendar}
          >
            <img className="Main-Page-Calendar-Img" src={Cal} />
            ปฎิทินกิจกรรม
          </button>
        </div>
      </div>
      <div>
        <Modal
          show={showModalCalendar}
          onHide={handleCloseModalCalendar}
          keyboard={false}
          size="xl"
        >
          <Modal.Header closeButton>
            <Modal.Title>ปฏิทินกิจกรรม</Modal.Title>
          </Modal.Header>
          <Calendar />
        </Modal>
      </div>
    </>
  );
};

export default MainPageSlidebar;
